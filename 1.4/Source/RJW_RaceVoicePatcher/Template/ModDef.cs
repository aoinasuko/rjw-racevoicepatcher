﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RJW_RaceVoicePatcher
{
    public class RaceVoiceDef : Def
    {
        // 対象の種族のDefName
        public List<String> Race = new List<String>();

        // 対象の種族の喘ぎ声のDefName(女性)
        public List<SoundDef> Voice_Female = new List<SoundDef>();

        // 対象の種族の喘ぎ声のDefName(男性)
        public List<SoundDef> Voice_Male = new List<SoundDef>();

        // 対象の種族の絶頂声のDefName(女性)
        public List<SoundDef> Cum_Female = new List<SoundDef>();

        // 対象の種族の絶頂声のDefName(男性)
        public List<SoundDef> Cum_Male = new List<SoundDef>();
    }
}
