﻿using AlienRace;
using HarmonyLib;
using Rimworld_Animations;
using rjw;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.Sound;

namespace RJW_RaceVoicePatcher
{
    [StaticConstructorOnStartup]
    static class List_Voice
    {
        public static Dictionary<String, bool> racelist = new Dictionary<string, bool>();

        static List_Voice()
        {
            // 種族が専用のボイスを持っているかリストに入れる
            foreach (ThingDef_AlienRace racedef in DefDatabase<ThingDef_AlienRace>.AllDefsListForReading)
            {
                racelist.Add(racedef.defName, false);
                foreach (RaceVoiceDef voicedef in DefDatabase<RaceVoiceDef>.AllDefsListForReading)
                {
                    Log.Message(racedef.defName + ": " + voicedef.Race.Contains(racedef.defName).ToString());
                    if (voicedef.Race.Contains(racedef.defName))
                    {
                        racelist.Remove(racedef.defName);
                        racelist.Add(racedef.defName, true);
                        break;
                    }
                }
            }
        }
    }

    [StaticConstructorOnStartup]
    static class Harmony
    {
        static Harmony()
        {
            var harmony = new HarmonyLib.Harmony("BEP.RJW.RaceVoicePatcher");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(CompBodyAnimator), "tickClip")]
    static class Fix_CompBodyAnimator
    {

        [HarmonyPrefix]
        static bool Prefix(ref CompBodyAnimator __instance, ref int ___clipTicks, ref bool ___quiver, ref float ___clipPercent, ref int ___curStage, ref int ___actor, ref AnimationDef ___anim)
        {
            // もしポーンのボイスがある場合は独自処理に移行する
            String pawn_defname = __instance.pawn.def.defName;
            if (List_Voice.racelist.TryGetValue(pawn_defname, false) == true)
            {
                ___clipTicks++;
                AnimationStage stage = ___anim.animationStages[___curStage];
                PawnAnimationClip clip = (PawnAnimationClip)stage.animationClips[___actor];

                if (rjw.RJWSettings.sounds_enabled && clip.SoundEffects.ContainsKey(___clipTicks) && AnimationSettings.soundOverride)
                {
                    Pawn instpawn = __instance.pawn;

                    // 声の一覧
                    List<SoundDef> linesound = new List<SoundDef>();
                    List<SoundDef> cumsound = new List<SoundDef>();
                    List<RaceVoiceDef> voicelist = DefDatabase<RaceVoiceDef>.AllDefsListForReading.Where(x => x.Race.Contains(pawn_defname)).ToList();

                    // ボイス一覧を追加する
                    foreach (RaceVoiceDef voicedef in voicelist)
                    {
                        if (instpawn.gender == Gender.Female)
                        {
                            linesound.AddRange(voicedef.Voice_Female);
                            cumsound.AddRange(voicedef.Cum_Female);
                        } else
                        {
                            linesound.AddRange(voicedef.Voice_Male);
                            cumsound.AddRange(voicedef.Cum_Male);
                        }
                    }

                    SoundInfo sound = new TargetInfo(instpawn.Position, instpawn.Map);
                    string soundEffectName = clip.SoundEffects[___clipTicks];
                    SoundDef soundEffect = null;

                    if ((instpawn.jobs.curDriver as JobDriver_Sex).isAnimalOnAnimal)
                    {
                        sound.volumeFactor *= RJWSettings.sounds_animal_on_animal_volume;
                    }

                    // 通常時の声
                    if (soundEffectName.Contains("Voiceline"))
                    {
                        soundEffect = linesound.RandomElement();
                        sound.volumeFactor *= RJWSettings.sounds_voice_volume;
                    }

                    // 絶頂時の声
                    if (clip.SoundEffects[___clipTicks].Contains("Cum"))
                    {
                        soundEffect = cumsound.RandomElement();
                        sound.volumeFactor *= RJWSettings.sounds_cum_volume;
                        __instance.considerApplyingSemen();
                    }
                    else
                    {
                        sound.volumeFactor *= RJWSettings.sounds_sex_volume;
                    }
                    if (soundEffect != null)
                    {
                        soundEffect.PlayOneShot(sound);
                    } else
                    {
                        SoundDef.Named(soundEffectName).PlayOneShot(sound);
                    }
                }
                if (AnimationSettings.orgasmQuiver && clip.quiver.ContainsKey(___clipTicks))
                {
                    ___quiver = clip.quiver[___clipTicks];
                }

                if (___clipPercent >= 1 && stage.isLooping)
                {
                    ___clipTicks = 1;
                }
                ___clipPercent = (float)___clipTicks / (float)clip.duration;

                __instance.calculateDrawValues();

                return false;
            }
            return true;
        }

    }
}
