＊日本語説明

「これは何？」

RimJobWorldのアドオン、Rimworld-Animationsのアニメーション時の喘ぎ声を種族(と性別)毎に変更することが出来るようになるMODです。
詳細な説明は、同梱のDefファイルを閲覧ください。LittleFairy用のサンプルが入っています。

「前提MODは？」

RimJobWorld Rimworld-Animations Humanoid Alien Races

「このMODを改造していいの？改造したのを公開していいの？」

もちろんです！ このMODはMITライセンスで作成されています。 （できれば、参考にしたものとしてここのリンクを貼ってくれると嬉しいな…）

＊English Description

"What is this?"

This is a mod for RimJobWorld's add-on, Rimworld-Animations, that allows you to change the wheezing voice during animation for each race (and gender).
For detailed instructions, please see the included def file, which contains a sample for LittleFairy.
(Sorry, the explanation in the sample is in Japanese only.)

"What are the prerequisite mods?"

RimJobWorld Rimworld-Animations Humanoid Alien Races

"Can I modify this mod? Can I publish the mod?"

Of course! This mod is created under the MIT license. (But I'd appreciate it if you could post a link here as a reference if you can...)
